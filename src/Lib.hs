{-# LANGUAGE DeriveAnyClass,DeriveGeneric,FlexibleInstances #-}
module Lib
    (
      ixBoard,
      updateIxBoard,
      defaultBoard,
      printBoard,
      isWin,
      isEven,
      PlayerName,
      PlayerCommands (..),
      ServerCommands (..),
      PlayerCommandsInGame (..),
      ServerCommandsInGame (..),
      Mark (X,O),
      Coords (..),
      Board,
      Auth (..)
    ) where

import Data.Serialize
import Data.Vector (Vector,(!),modify,replicate)
import qualified Data.Vector as V
import Data.Vector.Mutable (write)
import GHC.Generics
import Data.Text (Text)
import Data.Serialize.Text ()
import Data.Vector.Serialize ()
import Text.Printf

data Mark = X | O | E deriving (Generic,Serialize,Eq,Show)
newtype Board = Board (Vector Mark) deriving (Generic,Serialize,Show)
newtype Coords =Coords {fromCoords ::(Int,Int)} deriving (Generic,Serialize)

type PlayerName = Text

data PlayerCommands = PlayWith PlayerName |
                      Players |
                      Quit |
                      WannaStartGameWith PlayerName |
                      MessageInMenu PlayerName Text deriving (Generic,Serialize)
data ServerCommands = Print PlayerName |
                      WannaPlayWith PlayerName |
                      StartGameWith PlayerName |
                      PlayerList [PlayerName] |
                      ReceiveMessageInMenu PlayerName Text |
                      Bye deriving (Generic,Serialize)
data PlayerCommandsInGame = Set Int Int |
                            Stop |
                            MessageInGame PlayerName Text deriving (Generic,Serialize)

data ServerCommandsInGame = Alert Text |
                            GameState Board |
                            YouWin |
                            YouLose |
                            ReceiveMessageInGame PlayerName Text |
                            Even deriving (Generic,Serialize)

newtype Auth = Auth PlayerName deriving (Generic,Serialize)

ixBoard :: Coords -> Int
ixBoard (Coords (r,c)) = (r-1)*3+(c-1)
updateIxBoard :: Coords -> Mark -> Board -> Board
updateIxBoard c m (Board v) = Board $ modify (\v' -> write v' (ixBoard c) m) v
defaultBoard :: Board
defaultBoard = Board $ Data.Vector.replicate 9 E

isWin :: Board -> Bool
isWin (Board b) = or (vertical++horizontal++diagonal) where
    check x y z = b ! x == b ! y && b ! y == b ! z && b ! x /= E --and [b ! i == b ! j && b ! i /= E | [i,j] <- mapM (const [x,y,z]) [1..2::Int], i < j]
    vertical = [check i (i+1) (i+2) | i <- [0,2,5]]
    horizontal = [check i (i+3) (i+6) | i <- [0,1,2]]
    diagonal = [check i 4 j | (i,j) <- [(0,8),(2,6)]]

isEven :: Board -> Bool
isEven (Board b) = not (V.any (== E) b)

isEmptyOnCoords :: Board -> Coords -> Bool
isEmptyOnCoords (Board b) coords = (b V.! ixBoard coords) == E

recapp :: (t1 -> t1 -> t1 -> t1 -> t1 -> t1 -> t1 -> t1 -> t1 -> t) -> [t1] -> t
recapp f [a,s,d,r,g,h,j,k,l] = f a s d r g h j k l
recapp _ _ = error "Wrong argument number"

markToChar :: Mark -> Char
markToChar X = 'X'
markToChar O = 'O'
markToChar E = ' '

printBoard :: Board -> IO ()
printBoard (Board b) = recapp pf [markToChar (b ! i) | i <- [0..8]] where
  pf :: Char -> Char -> Char -> Char -> Char -> Char -> Char -> Char -> Char -> IO ()
  pf = printf string
  string = unlines
    [
      "    |    |    ",
      "  %c |  %c |  %c ",
      "    |    |    ",
      "--------------",
      "    |    |    ",
      "  %c |  %c |  %c ",
      "    |    |    ",
      "--------------",
      "    |    |    ",
      "  %c |  %c |  %c ",
      "    |    |    "
    ]
