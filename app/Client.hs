{-# LANGUAGE OverloadedStrings #-}
module Client
    (
    main
    ) where

import Lib

import Network.WebSockets (receiveData,forkPingThread,sendBinaryData,sendClose,runClient,ClientApp)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Read as T
import Data.Serialize (encode,decode)
import Control.Concurrent (forkIO)
import Control.Monad
import System.IO.Unsafe (unsafePerformIO)
import Data.StateVar
import Data.IORef

data Mode = InGame | InMenu

{-# NOINLINE mode #-}
mode :: IORef Mode
mode = unsafePerformIO (newIORef InMenu)

{-# NOINLINE playingagainst #-}
playingagainst :: IORef Text
playingagainst = unsafePerformIO (newIORef T.empty)

receiver :: ClientApp ()
receiver conn = forever $ do
  mode' <- get mode
  case mode' of
    InGame -> ingame
    InMenu -> inmenu
  where
    inmenu = do
      c <- decode <$> receiveData conn
      case c of
        Left err -> putStrLn err
        Right x -> case x of
          Print t -> print t
          WannaPlayWith p -> T.putStrLn ("Player " `mappend` p  `mappend` " wants to play with you")
          StartGameWith pn -> do
            mode $= InGame
            playingagainst $= pn
            printBoard defaultBoard
          PlayerList pl -> mapM_ (\pn -> T.putStr pn >> T.putStr " ") pl >> T.putStrLn ""
          ReceiveMessageInMenu pn msg -> T.putStr pn >> T.putStr ": " >> T.putStrLn msg
          Bye -> undefined

    ingame = do
      c <- decode <$> receiveData conn
      case c of
        Left err -> putStrLn err
        Right x -> case x of
          Alert e -> T.putStrLn e
          GameState gs -> printBoard gs
          YouWin -> do
            putStrLn "You won!"
            mode $= InMenu
          YouLose -> do
            putStrLn "You lose!"
            mode $= InMenu
          Even -> do
            putStrLn "Even"
            mode $= InMenu
          ReceiveMessageInGame pn msg -> T.putStr pn >> T.putStr ": " >> T.putStrLn msg


userinputhandler :: ClientApp ()
userinputhandler conn = forever $ do
  line <- T.getLine
  mode' <- get mode
  case mode' of
    InGame -> ingame line
    InMenu -> inmenu line
  where
    inmenu l =
      case T.words l of
        [":players"] -> sendBinaryData conn (encode Players)
        [":playwith",pn] -> sendBinaryData conn (encode $ PlayWith pn)
        [":startgamewith",pn] -> sendBinaryData conn (encode $ WannaStartGameWith pn) --TODO message handler
        [":quit"] -> sendClose conn (encode Quit)
        _ -> putStrLn ":players :playwith :startgamewith :quit"

    ingame l =
      case T.words l of
        [":set",x,y] -> case (T.decimal x,T.decimal y) of
          (Right (x',_),Right (y',_)) ->
            sendBinaryData conn (encode $ Set (fromInteger x') (fromInteger y'))
          _ -> putStrLn "Wrong input"
        [":surrender"] -> do
          sendBinaryData conn (encode Stop)
          mode $= InMenu
        _ -> putStrLn ":surrender :set x y"

mainmenu :: ClientApp ()
mainmenu conn = do
  forkPingThread conn 15
  T.putStrLn "Enter name: "
  name <- T.getLine
  sendBinaryData conn $ encode $ Auth name
  _ <- forkIO $ userinputhandler conn
  receiver conn

main :: IO ()
main = do
  putStrLn "Enter server ip (empty = localhost): "
  ip <- getLine
  if ip == ""
  then runClient "localhost" 8080 "" mainmenu
  else runClient ip 8080 "" mainmenu
