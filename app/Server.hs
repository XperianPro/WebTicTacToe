{-# LANGUAGE OverloadedStrings #-}
module Server
    (
    main
    ) where

import Lib

import Control.Concurrent.MVar
import Data.Map (Map,insert,(!),toList,delete,lookup,alter)
import Prelude hiding (lookup)
import System.IO.Unsafe (unsafePerformIO)
import Control.Concurrent (forkIO)
import Network.WebSockets (receiveData,sendBinaryData,acceptRequest,defaultConnectionOptions,sendClose,Connection,ServerApp)
import Data.Text (Text)
import qualified Data.Text as T (empty)
import Data.Serialize (encode,decode)
import Data.ByteString ()
import Network.Wai.Handler.WebSockets
import Network.Wai
import Network.Wai.Handler.Warp (run)
import Control.Monad
import Network.HTTP.Types.Status
import Control.Exception (onException)
import Data.Tuple (swap)

type InGame = Bool
type PlayerStates = Map PlayerName PlayerState
data PlayerState = PlayerState {getConn :: Connection , isInGame :: InGame, wannaPlayWith :: Text}
data GState = GState {board :: Board, opponents :: (PlayerName,PlayerName), turn :: Text} deriving (Show)

instance Show PlayerState where
  show x = "isInGame " ++ show (isInGame x) ++ " wannaPlayWith " ++ show (wannaPlayWith x)

defaultGState :: PlayerName -> PlayerName -> GState
defaultGState n1 n2 = GState defaultBoard (n1,n2) n1
defaultPlayerState :: Connection -> PlayerState
defaultPlayerState conn = PlayerState conn False T.empty

{-# NOINLINE playerStates #-}
playerStates :: MVar PlayerStates
playerStates = unsafePerformIO $ newMVar mempty

{-# NOINLINE gameStates #-}
gameStates :: MVar (Map PlayerName (MVar GState))
gameStates = unsafePerformIO $ newMVar mempty

sendReq :: PlayerName -> PlayerName -> IO ()
sendReq curname name = do
  states <- takeMVar playerStates
  let (PlayerState conn ingame _) = states ! name
  if not ingame
    then do
      changePlayerWannaPlayWith states name
      sendBinaryData conn (encode $ WannaPlayWith curname)
    else putMVar playerStates states
  where
    changePlayerWannaPlayWith :: PlayerStates -> PlayerName -> IO ()
    changePlayerWannaPlayWith st pn =
      putMVar playerStates (alter (\(Just ps) -> Just $ ps {wannaPlayWith = pn}) curname st)

communicator :: ServerApp
communicator pendingconn = do
  conn <- acceptRequest pendingconn
  auth <- decode <$> receiveData conn
  case auth of
    Left err -> do
      putStrLn err
      sendClose conn (encode Bye)
    Right (Auth name) -> do
      print $ name `mappend` " joined"
      modifyMVar_ playerStates (return.insert name (defaultPlayerState conn))
      sendBinaryData conn (encode $ Print $ "Welcome " `mappend` name)
      deletePlayer name $ forever $ do
        e <- receiveData conn
        ps <- readMVar playerStates
        if isInGame (ps ! name)
          then ingame conn name e
          else inmenu conn name e
  where
    ingame conn name e' = do
      let e = decode e'
      case e of
        Left err -> putStrLn err
        Right c -> case c of
          Set x y -> do
            gs'@(GState board' _ turn') <- readGameState
            if turn' /= name
              then if and [i > 0 && i <= 3 | i <- [x,y]] && isEmptyOnCoords (Coords (x,y))
                then do
                  let updatedBoard = updateIxBoard (Coords (x,y)) (mark gs') board'
                  modifyGameState $ return.const gs' {
                    board = updatedBoard,
                    turn = name
                  }
                  sendBoardsAndWinCheck
                else sendBinaryData conn $ encode $ Alert "Wrong coords"
              else sendBinaryData conn $ encode $ Alert "Not your turn"
          MessageInGame pn msg -> do
            ps <- readMVar playerStates
            case lookup pn ps of
              Just x -> sendBinaryData (getConn x) (encode $ ReceiveMessageInGame name msg)
              _ -> sendBinaryData conn (encode $ Alert "Player does not exist")
          Stop -> undefined
      where
        mark gs = if fst (opponents gs) == name then X else O
        readGameState = do
          gs <- readMVar gameStates
          let gsmvar = gs ! name
          readMVar gsmvar

        modifyGameState f = do
          gs <- readMVar gameStates
          let gsmvar = gs ! name
          modifyMVar_ gsmvar f

        sendBoardsAndWinCheck = do
          ps <- readMVar playerStates
          gs <- readGameState
          let conn1 = getConn $ ps ! fst (opponents gs)
              conn2 = getConn $ ps ! snd (opponents gs)
          sendBinaryData conn1 (encode $ GameState $ board gs)
          sendBinaryData conn2 (encode $ GameState $ board gs)
          if isWin (board gs)
          then do
            let (p1,p2) = (if turn gs == fst (opponents gs)
                            then id
                            else swap) (YouWin,YouLose)
            resetPlayerStates gs conn1 conn2
            sendBinaryData conn1 (encode p1)
            sendBinaryData conn2 (encode p2)
          else when (isEven (board gs)) $ do
            resetPlayerStates gs conn1 conn2
            sendBinaryData conn1 (encode Even)
            sendBinaryData conn2 (encode Even)
          where
            resetPlayerStates gs conn1 conn2 = modifyMVar_ playerStates $ return.
              insert (fst (opponents gs)) (defaultPlayerState conn1).
                insert (snd (opponents gs)) (defaultPlayerState conn2)

    inmenu conn name e' = do
      let e = decode e'
      case e of
        Left err -> putStrLn err
        Right c -> case c of
          Players -> do
            print $ name `mappend` " requsting player list"
            players <- readMVar playerStates
            sendBinaryData conn $ encode $ PlayerList $ map fst (toList players)
          PlayWith playername ->
            sendReq name playername
          WannaStartGameWith playername -> do
            ps <- takeMVar playerStates
            case lookup playername ps of
              Just x ->
                if wannaPlayWith x == name
                  then do
                    initgame name playername
                    sendBinaryData conn $ encode $ StartGameWith playername
                    sendBinaryData (getConn x) $ encode $ StartGameWith name
                    putMVar playerStates $
                      changePlayerState name True $
                        changePlayerState playername True ps
                  else sendBinaryData conn $ encode $ Print "Player does not want to play with you,send him request"
              Nothing -> sendBinaryData conn $ encode $ Print "Player does not exist"
            _ <- tryPutMVar playerStates ps
            return ()
          MessageInMenu pn msg -> do
            ps <- readMVar playerStates
            case lookup pn ps of
              Just x -> if not (isInGame x)
                then sendBinaryData (getConn x) $ encode $ ReceiveMessageInMenu name msg
                else sendBinaryData conn $ encode $ Print "Player in game"
              _ -> sendBinaryData conn $ encode $ Print "Player not found"
          Quit -> do
             modifyMVar_ playerStates (return.delete name)
             sendClose conn (encode Bye)

deletePlayer :: PlayerName -> IO a -> IO a
deletePlayer name ex = onException ex $ do
  ps <- takeMVar playerStates
  gs <- takeMVar gameStates
  putMVar playerStates (delete name ps)
  putMVar gameStates (delete name gs)

changePlayerState :: PlayerName -> InGame -> PlayerStates -> PlayerStates
changePlayerState pn st ps =
 alter (\(Just ps') -> Just $ ps' {isInGame = st}) pn ps

initgame :: PlayerName -> PlayerName -> IO ()
initgame n1 n2 = do
  gs <- takeMVar gameStates
  mvar <- newMVar (defaultGState n1 n2)
  putMVar gameStates $ insert n1 mvar $ insert n2 mvar gs

main :: IO ()
main = do
  _ <- forkIO $ forever debug
  run 8080 $ websocketsOr defaultConnectionOptions communicator httpresponder

debug :: IO ()
debug = do
  l <- getLine
  case words l of
    [":players"] -> do
      players <- readMVar playerStates
      print $ map fst (toList players)
    [":playerstates"] -> do
      ps <- readMVar playerStates
      print ps
    [":gamestates"] -> do
      gs <- readMVar gameStates
      gss <- mapM (readMVar.snd) (toList gs)
      print gss
    _ -> putStrLn "Error"

httpresponder :: Application
httpresponder _ respond =
  respond $ responseLBS status200 [] "Only websocket supported"
